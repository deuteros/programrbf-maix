EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2200 1750 2200 1050
Wire Wire Line
	2200 1050 3400 1050
Wire Wire Line
	4650 1050 4650 2750
Wire Wire Line
	3550 3550 3550 3650
Wire Wire Line
	3550 3650 2900 3650
Wire Wire Line
	4050 4250 3850 4250
NoConn ~ 4750 2750
NoConn ~ 4950 2750
NoConn ~ 5050 2750
NoConn ~ 5650 3150
NoConn ~ 5650 3250
NoConn ~ 5650 3450
NoConn ~ 5650 3550
NoConn ~ 5650 3850
NoConn ~ 5650 3950
NoConn ~ 5650 4150
NoConn ~ 5650 4750
NoConn ~ 5650 4850
NoConn ~ 4050 4650
NoConn ~ 4050 4550
NoConn ~ 4050 4450
NoConn ~ 4050 4350
NoConn ~ 4050 4050
NoConn ~ 4050 3950
NoConn ~ 4050 3850
NoConn ~ 4050 3650
NoConn ~ 4050 3450
NoConn ~ 2900 1950
NoConn ~ 2900 2050
$Comp
L adapter-rescue:Sipeed-Maix-Bit-MCUs-adapter-rescue A1
U 1 1 60CFA58F
P 2300 3750
F 0 "A1" H 1656 3846 50  0000 R CNN
F 1 "Sipeed-Maix-Bit" H 1656 3755 50  0000 R CNN
F 2 "deuteros:Sipped-Maix-Bit" H 2700 1900 50  0001 C CNN
F 3 "https://dl.sipeed.com/MAIX/HDK/Sipeed-M1&M1W/Specifications" H 2000 4000 50  0001 C CNN
	1    2300 3750
	1    0    0    -1  
$EndComp
NoConn ~ 2900 2250
NoConn ~ 2900 2150
NoConn ~ 2900 2550
NoConn ~ 2900 2650
NoConn ~ 2900 2750
NoConn ~ 2900 2850
NoConn ~ 2900 2950
NoConn ~ 2900 3250
NoConn ~ 2900 3350
NoConn ~ 2900 3450
NoConn ~ 2900 3550
NoConn ~ 2900 4050
NoConn ~ 2900 4150
NoConn ~ 2900 4250
NoConn ~ 2900 4350
NoConn ~ 2900 4450
NoConn ~ 2900 4950
NoConn ~ 2900 5050
NoConn ~ 2900 5150
NoConn ~ 2900 5250
NoConn ~ 2900 5350
NoConn ~ 2900 5450
NoConn ~ 1700 2150
NoConn ~ 2300 1750
$Comp
L power:GND #PWR0101
U 1 1 60D1062F
P 4750 6250
F 0 "#PWR0101" H 4750 6000 50  0001 C CNN
F 1 "GND" H 4755 6077 50  0000 C CNN
F 2 "" H 4750 6250 50  0001 C CNN
F 3 "" H 4750 6250 50  0001 C CNN
	1    4750 6250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 60D11156
P 3400 800
F 0 "#PWR0102" H 3400 650 50  0001 C CNN
F 1 "+5V" H 3415 973 50  0000 C CNN
F 2 "" H 3400 800 50  0001 C CNN
F 3 "" H 3400 800 50  0001 C CNN
	1    3400 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 800  3400 1050
Connection ~ 3400 1050
Wire Wire Line
	3400 1050 4650 1050
Wire Wire Line
	4450 6050 4450 5350
Wire Wire Line
	4050 4750 3900 4750
Wire Wire Line
	4050 3550 3550 3550
Text GLabel 3700 3150 0    50   Input ~ 0
D16_PI_RX
Wire Wire Line
	3700 3150 4050 3150
$Comp
L Connector:Raspberry_Pi_2_3 J1
U 1 1 60CF06AB
P 4850 4050
F 0 "J1" H 4850 5531 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 4850 5440 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 4850 4050 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 4850 4050 50  0001 C CNN
	1    4850 4050
	1    0    0    -1  
$EndComp
Text GLabel 3700 3300 0    50   Input ~ 0
D15_PI_TX
Wire Wire Line
	3700 3300 4050 3300
Wire Wire Line
	4050 3300 4050 3250
Text GLabel 5950 4250 2    50   Input ~ 0
C15_PI_CS
Wire Wire Line
	5950 4250 5650 4250
Text GLabel 5950 4350 2    50   Input ~ 0
F15_PI_MISO
Text GLabel 5950 4450 2    50   Input ~ 0
F16_PI_MOSI
Text GLabel 5950 4550 2    50   Input ~ 0
F13_PI_CLK
Wire Wire Line
	5950 4350 5650 4350
Wire Wire Line
	5950 4450 5650 4450
Wire Wire Line
	5950 4550 5650 4550
Wire Wire Line
	6700 3750 6700 5550
Wire Wire Line
	5650 3750 6700 3750
Text GLabel 3100 4550 2    50   Input ~ 0
F15_PI_MISO
Text GLabel 3100 4650 2    50   Input ~ 0
F13_PI_CLK
Text GLabel 3100 4750 2    50   Input ~ 0
F16_PI_MOSI
Text GLabel 3100 4850 2    50   Input ~ 0
C15_PI_CS
Wire Wire Line
	3900 4500 3900 4750
Wire Wire Line
	3850 3750 3850 4250
Wire Wire Line
	2900 3750 3850 3750
Wire Wire Line
	3800 3950 3800 4500
Wire Wire Line
	2900 3950 3800 3950
Wire Wire Line
	3800 4500 3900 4500
Wire Wire Line
	3650 3850 3650 5550
Wire Wire Line
	2900 3850 3650 3850
Wire Wire Line
	3650 5550 6700 5550
Wire Wire Line
	3100 4550 2900 4550
Wire Wire Line
	2900 4650 3100 4650
Wire Wire Line
	3100 4750 2900 4750
Wire Wire Line
	3100 4850 2900 4850
Text GLabel 3200 2350 2    50   Input ~ 0
D16_PI_RX
Text GLabel 3200 2450 2    50   Input ~ 0
D15_PI_TX
Wire Wire Line
	3200 2350 2900 2350
Wire Wire Line
	3200 2450 2900 2450
Wire Wire Line
	4550 5350 4550 6050
Wire Wire Line
	4550 6050 4450 6050
Wire Wire Line
	4650 5350 4650 6050
Wire Wire Line
	4650 6050 4550 6050
Connection ~ 4550 6050
Wire Wire Line
	4750 5350 4750 6050
Wire Wire Line
	4750 6050 4650 6050
Connection ~ 4650 6050
Wire Wire Line
	4850 5350 4850 6050
Wire Wire Line
	4850 6050 4750 6050
Connection ~ 4750 6050
Wire Wire Line
	4950 5350 4950 6050
Wire Wire Line
	4950 6050 4850 6050
Connection ~ 4850 6050
Wire Wire Line
	5050 5350 5050 6050
Wire Wire Line
	5050 6050 4950 6050
Connection ~ 4950 6050
Wire Wire Line
	5150 5350 5150 6050
Wire Wire Line
	5150 6050 5050 6050
Connection ~ 5050 6050
Wire Wire Line
	4750 6050 4750 6250
$Comp
L power:GND #PWR01
U 1 1 60D743E1
P 2300 6200
F 0 "#PWR01" H 2300 5950 50  0001 C CNN
F 1 "GND" H 2305 6027 50  0000 C CNN
F 2 "" H 2300 6200 50  0001 C CNN
F 3 "" H 2300 6200 50  0001 C CNN
	1    2300 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 5650 2300 6200
$EndSCHEMATC
