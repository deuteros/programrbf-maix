#!/bin/bash

apt-get update
cd ~

#install Risc-V gcc
apt-get install g++-riscv64-linux-gnu gcc-riscv64-linux-gnu

# Install arduino-cli
apt-get install curl -y
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar xjf arduino-cli.tar.bz2
rm arduino-cli.tar.bz2
mv `ls -1` /usr/bin/arduino-cli

# Install python, pip and pyserial
apt-get install python -y
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py
python get-pip.py
pip install pyserial

# Install Maixduino core
printf "board_manager:\n  additional_urls:\n    - http://dl.sipeed.com/MAIX/Maixduino/package_Maixduino_k210_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml
arduino-cli core install Maixduino:k210 --config-file .arduino-cli.yaml

# Configure LV library to be able to use other themes
sed -i 's/#define LV_THEME_LIVE_UPDATE    0/#define LV_THEME_LIVE_UPDATE    1/g' ~/.arduino15/packages/Maixduino/hardware/k210/0.3.11/libraries/lv_maixduino/lv_conf.h
sed -i 's/#define LV_USE_THEME_NIGHT      0/#define LV_USE_THEME_NIGHT      1/g' ~/.arduino15/packages/Maixduino/hardware/k210/0.3.11/libraries/lv_maixduino/lv_conf.h

# Install 'native' packages
arduino-cli lib install "GeekFactory Shell Library"
cd -

# Install 'third-party' packages: find proper location and 'git clone'
apt-get install git -y
cd `arduino-cli config dump | grep sketchbook | sed 's/.*\ //'`/libraries
git clone https://github.com/deuteros76/SdFat.git
cd -
