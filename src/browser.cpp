#include "browser.h"

FsFile  sdfile, root;

browserObjectEvent onFileOpen;
NodeList *nodes;
NodeList *directories;
NodeList *files;

void browserShowDirectoryFiles(char dir[FILE_NAME_LENGTH]){
  char fileName[FILE_NAME_LENGTH];
  extern FileList fileList;
  
  root.getName(fileName,FILE_NAME_LENGTH);

  if (!root.open(dir)){
    printf("Error opening directory %s %s", fileName, dir);    
  }

  fileList.clean();
  linkedListClear(files);
  files = NULL;
  linkedListClear(directories);
  directories = NULL;
  
  if (root.isSubDir()){
    fileList.listAddDirectory("..");
  }
  while (sdfile.openNext(&root, O_RDONLY)) {
    if (!sdfile.isHidden() && sdfile.getName(fileName,FILE_NAME_LENGTH)){
      //shell_println(fileName);
      if (!sdfile.isDirectory()) {
        const char *ext = strrchr(fileName, '.');
        if (ext && strcasecmp(ext, ".RBF") == 0) {
          //listAddItem(listCores, fileName);
          files=linkedListAddNode(files,fileName);
        }
      }else{
          //listAddDirectory(listCores, fileName);
          directories=linkedListAddNode(directories,fileName);
        }
    }
  }
  
  sdfile.close();

  linkedListSort(&files);
  linkedListSort(&directories);
  
  NodeList *node=directories;
  while (node != NULL){
    fileList.listAddDirectory(node->name);  
    node=node->next;
  }

  node=files;
  while (node != NULL){
    fileList.listAddItem(node->name);  
    node=node->next;
  }
  fileList.listSelectTopItem();
}

void browserOpenResource(void * ptr, int val){
  extern FileList fileList;
  char fileName[FILE_NAME_LENGTH];  
  const char *itemName = fileList.getCurrentItemText();

  Serial.println (itemName);
  sdfile.open(&root,itemName);

  if (!sdfile) {
    root.getName(fileName,FILE_NAME_LENGTH);
    if (strcmp(itemName,"..")==0){
      char *path=browserGetParentDir();  
      linkedListRemoveNode(nodes);
      browserShowDirectoryFiles(path);
    }else {
      printf ("error: file open failed for %s %s.\n",fileName, itemName);
    }

  }else if (sdfile.isDirectory()){
     sdfile.getName(fileName,FILE_NAME_LENGTH);
  
     char *path=browserGetCurrentPath();  
      
     path= (char *) realloc(path, (strlen(path)+strlen(fileName)+2)*sizeof(char));
     strcat(path,fileName);
     // Add this directory to the linked list storing the absolute path
     nodes=linkedListAddNode(nodes,fileName);  
     browserShowDirectoryFiles(path);
     
  }else{
      onFileOpen(ptr, val);
  }
  sdfile.close();
}

char *browserGetCurrentPath(){     
  char * path=(char *)malloc(sizeof(char));
  strcpy(path,"");
  NodeList *node=nodes;
  while (node != NULL){
    path=(char *)realloc(path, (strlen(path)+strlen(node->name)+2)*sizeof(char));
    strcat(path,node->name);
    strcat(path,"/");
    node=node->next;
  }
  return path;
}


char *browserGetParentDir(){     
  char * path=(char *)malloc(sizeof(char));
  strcpy(path,"");
  NodeList *node=nodes;
  while (node->next != NULL){
    path=(char *)realloc(path, (strlen(path)+strlen(node->name)+2)*sizeof(char));
    strcat(path,node->name);
    strcat(path,"/");
    node=node->next;
  }
  return path;
}
