/**
  BSD 3-Clause License

  Copyright (c) 2021, Guillermo Amat
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#ifndef BROWSER_H
#define BROWSER_H

#include <SdFat.h>
#include <sdios.h>
#include <strings.h>
#include "gui/maixgui.h"
#include "linkedlist.h"

#define FILE_NAME_LENGTH 256 // Max length allowed by SdFat

#ifdef __cplusplus
extern "C" {
#endif

//! Function pointers to manage button events
typedef void (*browserObjectEvent)(void *, int);

extern browserObjectEvent onFileOpen;

extern NodeList *nodes; //! This list stores the curren path of the directory tree
extern NodeList *directories; //! Directories available in the curren path
extern NodeList *filees; //! Files available in the current path

extern FsFile  sdfile, root;

/** 
 * Shows the list of directories and files for the current path. Internally creates one list for directories
 * and another one for the files. Then, calls the sorting function and orthers them alphabetically
 * @param dir Path of the directory to be listed. Used as a full path but probably it should work using a relative oen (not tested)
 *
 **/ 
void browserShowDirectoryFiles(char dir[FILE_NAME_LENGTH]);    

/**
 * This method is used as an slot for other objects. It analyses the tyoe of item. If is a file it tries to send it as an RBF,
 * in case of being a directory it calls browserShowDirectoryFiles to enter in that folder. Lastly, if is the string ".." navigates
 * to the paren dir.
 * The paramenters of this method are reserved for being used if neede when used from another "object"
 * 
 * @param ptr pointer to generica data (not used)
 * @param val int value (not used)
 * 
 **/
void browserOpenResource(void * ptr, int val);

/** 
 * Returns the current path iterating the nodes list and creating a string to represent it.
 * @return the current path as a string
 * 
 **/
char * browserGetCurrentPath();


/** 
 * Returns the path to the parent directory iterating the nodes list, removing the last node and creating a string to represent 
 * the resultant route.
 * @return the path to the parent folder as a string
 * 
 **/
char * browserGetParentDir();
    
#ifdef __cplusplus
}
#endif

#endif //BROWSER_H 
 
