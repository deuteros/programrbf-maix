import os
import shutil

Import("env")

if env.get("PIOPLATFORM")=="raspberrypi":
  confDir = env.get("PIOENV")
  frameworkName = "framework-arduino-mbed"
  portenta_lvgl_dir = os.path.join(env.PioPlatform().get_package_dir(frameworkName),"libraries","Portenta_lvgl")
  if os.path.isfile(portenta_lvgl_dir):
    os.rmdir(portenta_lvgl_dir);
elif env.get("PIOPLATFORM")=="espressif32":
  confDir = "esp32"
  frameworkName = "framework-arduinoespressif32"
  
  # Patch Sheel.h to make it compatible with ESP32 https://github.com/geekfactory/Shell/pull/26
  original_file = os.path.join(env.get("PROJECT_LIBDEPS_DIR"),env.get("PIOENV"),"GeekFactory Shell Library","Shell.h")
  patched_file = os.path.join(env.get("PROJECT_SRC_DIR"),"patch", "shell.patch")
  #assert os.path.isfile(original_file) and os.path.isfile(patched_file)
  env.Execute("patch %s %s" % ( "\"" + original_file + "\"", patched_file))
 

st7735_config = os.path.join(env.get("PROJECT_SRC_DIR"),"configuration",confDir,"ST7735","User_Setup.h")
st7735_config_dest = os.path.join(env.get("PROJECT_LIBDEPS_DIR"),env.get("PIOENV"),"TFT_eSPI")
if not os.path.exists(st7735_config_dest):
    os.makedirs(st7735_config_dest)
st7735_config_dest = os.path.join(st7735_config_dest,"User_Setup.h")

lvgl_config = os.path.join(env.get("PROJECT_SRC_DIR"),"configuration",confDir,"lvgl","lv_conf.h")
lvgl_config_dest = os.path.join(env.get("PROJECT_LIBDEPS_DIR"),env.get("PIOENV"),"lv_conf.h")

if os.path.isfile(st7735_config):
  shutil.copyfile(st7735_config, st7735_config_dest)
else:
  print("Error copying st7355 configuration file "+st7735_config_dest)

if os.path.isfile(lvgl_config):
  shutil.copyfile(lvgl_config, lvgl_config_dest)
else:
  print("Error copying lvgl configuration file " + lvgl_config)

#sprint(env.Dump())
