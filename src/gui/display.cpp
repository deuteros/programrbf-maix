#include "display.h"

#ifdef DISPLAY_H
/* Null, because instance will be initialized on demand. */
Display* Display::displayInstance = 0;

Display* Display::getDisplay()
{
    if (displayInstance == 0)
    {
        displayInstance = new Display();
    }

    return displayInstance;
}

#ifdef K210
Display::Display(){
  SPIClass spi_(SPI0);// MUST be SPI0 for Maix series on board LCD
  lcd= new Sipeed_ST7789 (320, 240, spi_);
  lcd->begin(15000000, COLOR_WHITE);

  lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);
  //Initialize the display
  lv_disp_drv_init(&disp_drv);
  disp_drv.hor_res = 320;
  disp_drv.ver_res = 240;
  disp_drv.flush_cb = my_disp_flush;
  disp_drv.buffer = &disp_buf;
  lv_disp_drv_register(&disp_drv);

  //Initialize the touch pad
  /*
  lv_indev_drv_t indev_drv;
  lv_indev_drv_init(&indev_drv);
  indev_drv.type = LV_INDEV_TYPE_ENCODER;
  indev_drv.read_cb = read_encoder;
  lv_indev_drv_register(&indev_drv);
  */

  //Initialize the graphics library's tick
  tick.attach_ms(LVGL_TICK_PERIOD, lv_tick_handler);
}


/* Display flushing */
void Display::my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p) {
  int32_t w = area->x2 - area->x1 + 1;
  int32_t h = area->y2 - area->y1 + 1;
  int32_t x, y;
  int32_t i = 0;
  uint16_t* data = (uint16_t*)malloc( w * h * 2 );
  uint16_t* pixels = data;

  for (y = area->y1; y <= area->y2; ++y)
  {
    for (x = area->x1; x <= area->x2; ++x)
    {
      pixels[i++] = (color_p->ch.red << 3) | (color_p->ch.blue << 8) | (color_p->ch.green >> 3 & 0x07 | color_p->ch.green << 13);
      // or LV_COLOR_16_SWAP = 1
      ++color_p;
    }
  }
  Display::getDisplay()->lcd->drawImage((uint16_t)area->x1, (uint16_t)area->y1, (uint16_t)w, (uint16_t)h, data);
  free(data);
  lv_disp_flush_ready(disp); /* tell lvgl that flushing is done */
}
#else
Display::Display(){  
  tick = new TickTwo(lv_tick_handler,LVGL_TICK_PERIOD);

  tft =  new TFT_eSPI();  // Invoke library, pins defined in User_Setup.h
  tft->init();
  tft->setRotation(0);
  tft->fillScreen(TFT_GREY);
  
  lv_disp_draw_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);
  //Initialize the display
  lv_disp_drv_init(&disp_drv);
  disp_drv.hor_res = 128;
  disp_drv.ver_res = 160;
  disp_drv.flush_cb = my_disp_flush;
  disp_drv.draw_buf = &disp_buf;
  lv_disp_drv_register(&disp_drv);

  //Initialize the touch pad
  /*
  lv_indev_drv_t indev_drv;
  lv_indev_drv_init(&indev_drv);
  indev_drv.type = LV_INDEV_TYPE_ENCODER;
  indev_drv.read_cb = read_encoder;
  lv_indev_drv_register(&indev_drv);
  */
}


/* Display flushing */
void Display::my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p)
{
    uint32_t w = (area->x2 - area->x1 + 1);
    uint32_t h = (area->y2 - area->y1 + 1);

    Display::getDisplay()->tft->startWrite();
    Display::getDisplay()->tft->setAddrWindow(area->x1, area->y1, w, h);
    Display::getDisplay()->tft->pushColors(&color_p->full, w * h);
    Display::getDisplay()->tft->endWrite();

    lv_disp_flush_ready(disp);
}
#endif


/* Interrupt driven periodic handler */
void Display::lv_tick_handler(void)
{

  //lv_tick_inc(LVGL_TICK_PERIOD);
}

/* Reading input device (simulated encoder here) */
bool Display::read_encoder(lv_indev_drv_t * indev, lv_indev_data_t * data)
{
  static int32_t last_diff = 0;
  int32_t diff = 0; /* Dummy - no movement */

  data->enc_diff = diff - last_diff;;
  data->state = LV_INDEV_STATE_RELEASED; /* Dummy - no press */

  last_diff = diff;

  return false;
}

#endif /*DISPLAY_H*/