/**
  BSD 3-Clause License

  Copyright (c) 2021, Guillermo Amat
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#ifndef USE_SPI_HDMI

#ifndef DISPLAY_H
#define DISPLAY_H

#include <lvgl.h>
#include <TickTwo.h>
#ifdef K210 
#include <Sipeed_ST7789.h>
#else
#include <SPI.h>
#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#endif

#define LVGL_TICK_PERIOD 10

#define LV_STATE_PRESSED LV_BTN_STATE_PR
#define LV_STATE_DEFAULT LV_BTN_STATE_INA

class Display{
private:
  //SPIClass spi_(SPI0);// MUST be SPI0 for Maix series on board LCD
  TickTwo *tick; // timer for interrupt handler 
#ifdef K210
  Sipeed_ST7789 *lcd;//(320, 240, spi_);
#else 
  #define TFT_GREY 0xBDF7
  TFT_eSPI *tft;
#endif 

#ifndef LV_HOR_RES_MAX
#define LV_HOR_RES_MAX 128
#endif

  lv_disp_draw_buf_t disp_buf;
  lv_disp_drv_t disp_drv;
  lv_color_t buf[LV_HOR_RES_MAX * 10];

  //! Singleton properties
  static Display *displayInstance; //! The only instance
  Display(); //! Private constructor to avoid more instances
public:
  static void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p);
  static void lv_tick_handler(void);
  bool read_encoder(lv_indev_drv_t * indev, lv_indev_data_t * data);

  //! Singleton method to get the instance
  static Display* getDisplay();
};

#endif /* DISPLAY_H */
#endif /*USE_SPI_HDMI*/