#include "maixgui.h"

lv_obj_t * container = 0;
lv_obj_t * barSendingUpdate = 0;
lv_obj_t * labelResult = 0;
lv_obj_t * labelError = 0;

lv_style_t * style_btn = 0;


lv_obj_t *progressBarCreate(void)
{
    lv_obj_t * barSending = lv_bar_create(container);
    lv_obj_set_size(barSending, lv_disp_get_hor_res(NULL)-40, 20);
    lv_obj_align(barSending, LV_ALIGN_CENTER, 0, 0);
    //lv_bar_set_anim_time(barSending, 2000);
    lv_bar_set_value(barSending, 0, LV_ANIM_OFF);
    return barSending;
}


lv_obj_t *labelCreate(void)
{
    lv_obj_t * label = lv_label_create(container);
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_CENTER, 0);
    lv_label_set_recolor(label, true);    
    lv_obj_set_width(label, lv_disp_get_hor_res(NULL)-40);
    lv_label_set_long_mode(label, LV_LABEL_LONG_WRAP);     /*Break the long lines*/
    lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);
    return label;
}

lv_obj_t *labelErrorCreate(void)
{
    lv_obj_t * label = lv_label_create(lv_scr_act());
    lv_label_set_long_mode(label, LV_LABEL_LONG_WRAP);     /*Break the long lines*/
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_CENTER, 0);
    lv_label_set_recolor(label, true);    
    lv_obj_set_width(label, lv_disp_get_hor_res(NULL)-40);
    lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);

    return label;
}

void Gui::guiSetup() {

  lv_init();
  
  // This configures the display
  #ifdef USE_SPI_HDMI
  SPIDisplay::getDisplay();
  #else
  Display::getDisplay();
  #endif

  // Splash screen
  idleScreen=IdleScreen();

  labelError=labelErrorCreate();
}

void Gui::guiCreate() {
  extern FileList fileList;
  idleScreen.guiHideIdleScreen();  
  
  container = lv_obj_create(lv_scr_act());
  lv_obj_align(container, LV_ALIGN_TOP_LEFT, 0,0);
  lv_obj_set_flex_flow(container, LV_FLEX_FLOW_COLUMN);

  lv_obj_set_style_bg_color(lv_scr_act(), LV_COLOR_MAKE(0, 0, 0), LV_PART_MAIN);  
  
  /*Create a list*/
  fileList = FileList();
  barSendingUpdate=progressBarCreate();
  labelResult=labelCreate();
  
  lv_obj_add_flag(barSendingUpdate, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(barSendingUpdate, true);  
  lv_obj_add_flag(labelResult, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(labelResult, true);
  lv_obj_add_flag(labelError, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(labelError, true);
   
}

void Gui::guiShowError(const char *error){
  if (container){
    lv_obj_add_flag(container, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(container, true);
  }  
  idleScreen.guiHideIdleScreen();
  lv_obj_clear_flag(labelError, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(labelError, false);
  lv_label_set_text(labelError, error);
}
