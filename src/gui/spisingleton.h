/**
BSD 3-Clause License

Copyright (c) 2021, Guillermo Amat
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/
#ifndef SPI_SINGLETON_H
#define SPI_SINGLETON_H

#include <SPI.h>

#ifdef ESP32 
  #define HSPI_MISO   12
  #define HSPI_MOSI   13
  #define HSPI_SCLK   14
  #define HSPI_SS     15

  #define VSPI_MISO   MISO
  #define VSPI_MOSI   MOSI
  #define VSPI_SCLK   SCK
  #define VSPI_SS     SS
  #define WRITEPIN digitalWrite

  #define SPI_CLASS SPIClass
  #define SPI_CLASS_CONSTRUCTOR(A,B,C) SPIClass(HSPI)
#endif

#ifdef RASPBERRY_PI_PICO
  #define MISO 12
  #define MOSI 15
  #define SCK 14
  #define SS 13
  #define WRITEPIN gpio_put

  #define SPI_CLASS MbedSPI
  #define SPI_CLASS_CONSTRUCTOR MbedSPI
#endif

#define SPICLK 62000000

class SPISingleton: public SPI_CLASS{
private:
  //! Singleton properties
  static SPISingleton *spiInstance; //! The only instance
  SPISingleton(); //! Private constructor to avoid more instances

  uint8_t data; //! Stores the data returned by the slave during a transfer operation

public:
  //! Singleton method to get the instance
  static SPISingleton* getInstance();

  void setData(uint8_t slaveData){data=slaveData;}
  uint8_t getData(){return data;}
};

#endif //SPI_SINGLETON