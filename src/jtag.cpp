
// Copyright (c) 2017-2021 - Victor Trucco
//
// All rights reserved
//
// Redistribution and use in source and synthezised forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// Redistributions in synthesized form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// Neither the name of the author nor the names of other contributors may
// be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// You are responsible for any legal issues arising from your use of this code.
//
//
// Maix Bit version by Guillermo Amat. Thanks to Carlos Palmero and
// Marcus Andrade (bogermann) for the original sources
//
#include "jtag.h"

#include <Wire.h>
#include <Shell.h>

#include <SPI.h>
#include <SdFat.h>
#include <sdios.h>

jtagEvent onValueChanged;

int IRlen = 0;
int nDevices = 0;

struct codestr
{
  unsigned char onebit: 1;
  unsigned int manuf: 11;
  unsigned int size: 9;
  unsigned char family: 7;
  unsigned char rev: 4;
};

union
{
  unsigned long code = 0;
  codestr b;
} idcode;




void error( ) {
  shell_printf("JTAG ERROR!!!!");
}


void JTAG_clock()
{
  WRITEPIN(PIN_TCK, HIGH);
  WRITEPIN(PIN_TCK, LOW);
}

void TDI_HIGH()
{
  WRITEPIN(PIN_TDI, HIGH);
}

void TDI_LOW()
{
  WRITEPIN(PIN_TDI, LOW);

}

void jtagSetup( ) {
  pinMode( PIN_TCK, OUTPUT );
  pinMode( PIN_TDO, INPUT_PULLUP );
  pinMode( PIN_TMS, OUTPUT );
  pinMode( PIN_TDI, OUTPUT );

  WRITEPIN( PIN_TCK, LOW );
  WRITEPIN( PIN_TMS, LOW );
  WRITEPIN( PIN_TDI, LOW );
}


void jtagRelease( ) {
  pinMode( PIN_TCK, INPUT_PULLUP );
  pinMode( PIN_TDO, INPUT_PULLUP );
  pinMode( PIN_TMS, INPUT_PULLUP );
  pinMode( PIN_TDI, INPUT_PULLUP );
}


void JTAG_reset()
{
  int i;

  WRITEPIN(PIN_TMS, HIGH);

  // go to reset state
  for (i = 0; i < 10; i++)
  {
    JTAG_clock();
  }
}


void JTAG_EnterSelectDR()
{
  // go to select DR
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
}

void JTAG_EnterShiftIR()
{
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();

}

void JTAG_EnterShiftDR()
{
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();

  // WRITEPIN(PIN_TMS, LOW); JTAG_clock(); //extra ?
}

void JTAG_ExitShift()
{
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
}

void JTAG_ReadData(int bitlength)
// note: call this function only when in shift-IR or shift-DR state
{
  int bitofs = 0;
  idcode.code = 0;
  unsigned long temp = 0;

  shell_println("Reading DR data");

  bitlength--;
  while (bitlength--)
  {
    WRITEPIN(PIN_TCK, HIGH);

    temp = digitalRead(PIN_TDO);

    //shell_printf("%u",temp);

    temp = temp << bitofs ;
    idcode.code |= temp;

    WRITEPIN(PIN_TCK, LOW);
    bitofs++;


  }

  WRITEPIN(PIN_TMS, HIGH);
  WRITEPIN(PIN_TCK, HIGH);

  temp = digitalRead(PIN_TDO);
  //shell_printf("%u",temp);
  //shell_println("");

  temp = temp << bitofs ;
  idcode.code |= temp;

  WRITEPIN(PIN_TCK, LOW);

  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();  // go back to select-DR
}


void JTAG_ReadDR(int bitlength)
{
  JTAG_EnterShiftDR();
  JTAG_ReadData(bitlength);
}

int JTAG_DetermineChainLength(char* s)
{
  int i;

  // empty the chain (fill it with 0's)
  WRITEPIN(PIN_TDI, LOW);
  for (i = 0; i < MaxIR_ChainLength; i++) {
    WRITEPIN(PIN_TMS, LOW);
    JTAG_clock();
  }

  WRITEPIN(PIN_TCK, LOW);

  // feed the chain with 1's
  WRITEPIN(PIN_TDI, HIGH);
  for (i = 0; i < MaxIR_ChainLength; i++)
  {


    WRITEPIN(PIN_TCK, HIGH);

    if (digitalRead(PIN_TDO) == HIGH) break;

    WRITEPIN(PIN_TCK, LOW);
  }

  WRITEPIN(PIN_TCK, LOW);

  shell_printf("%c = %d\n", s, i);


  JTAG_ExitShift();
  return i;
}


int jtagScan()
{
  int i = 0;

  JTAG_reset();
  JTAG_EnterSelectDR();
  JTAG_EnterShiftIR() ;

  IRlen = JTAG_DetermineChainLength((char*)"tamanho do IR");

  JTAG_EnterShiftDR();
  nDevices = JTAG_DetermineChainLength((char*)"Qtd devices");

  if (IRlen == MaxIR_ChainLength || nDevices == MaxIR_ChainLength )
  {
    error();
    nDevices = 0;
    return 1;
  } else {
    if (nDevices > 0) {
      // read the IDCODEs (assume all devices support IDCODE, so read 32 bits per device)
      JTAG_reset();
      JTAG_EnterSelectDR();

      shell_printf("Devices found %d\n", nDevices);
      JTAG_ReadDR(32 * nDevices);

      for (i = 0; i < nDevices; i++)
      {
        //assert(idcode.b.onebit);  // if the bit is zero, that means IDCODE is not supported for this device
        //shell_printf("Device %d IDCODE: %08X (Manuf %03X, Part size %03X, Family code %02X, Rev %X)\n", i+1, idcode[i], idcode[i].manuf, idcode[i].size, idcode[i].family, idcode[i].rev);

        shell_printf("Device IDCODE: %d\n", idcode.code);

        shell_printf(" rev: %d\n", idcode.b.rev);

        shell_printf(" family: %d\n", idcode.b.family);

        shell_printf(" size: %d\n", idcode.b.size);

        shell_printf(" manuf:  %03X\n", idcode.b.manuf);

        shell_printf(" onebit: %d\n", idcode.b.onebit);

      }
    } else {
      return 1;
    }
  }
  return 0;

}

void JTAG_PREprogram()
{
  int n;

  JTAG_reset();
  JTAG_EnterSelectDR();
  JTAG_EnterShiftIR() ;

  //  WRITEPIN(PIN_TMS, LOW); JTAG_clock(); //extra ?

  // aqui o TMS jÃ¡ esta baixo, nao precisa de outro comando pra abaixar.

  // IR = PROGRAM =   00 0000 0010    // IR = CONFIG_IO = 00 0000 1101
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, HIGH); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW);
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();

  // aqui o modo Ã© exit IR

  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();

  // aqui o modo Ã© update IR

  // Drive TDI HIGH while moving to SHIFTDR */
  WRITEPIN(PIN_TDI, HIGH);

  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();

  // aqui o modo Ã© select dr scan

  WRITEPIN(PIN_TMS, LOW); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();

  // aqui o modo estÃ¡ em shift dr

  //WRITEPIN(PIN_TMS, LOW); JTAG_clock(); //extra ?


  /* Issue MAX_JTAG_INIT_CLOCK clocks in SHIFTDR state */
  WRITEPIN(PIN_TDI, HIGH);
  for (n = 0; n < 300; n++)
  {
    JTAG_clock();
  }

  WRITEPIN(PIN_TDI, LOW);
}


void JTAG_POSprogram()
{

  int n;

  //aqui esta no exit DR

  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();

  // aqui esta no update DR

  WRITEPIN(PIN_TMS, LOW); JTAG_clock();

  //Aqui esta no RUN/IDLE

  JTAG_EnterSelectDR();
  JTAG_EnterShiftIR();

  // aqui em shift ir


  // IR = CHECK STATUS = 00 0000 0100
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, HIGH); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW);
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();


  //aqui esta no exit IR
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();

  //aqui esta no select dr scan




  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();

  //   aqui esta no shift IR


  // IR = START = 00 0000 0011
  WRITEPIN(PIN_TDI, HIGH); JTAG_clock();
  WRITEPIN(PIN_TDI, HIGH); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();
  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW); JTAG_clock();

  WRITEPIN(PIN_TDI, LOW);
  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();


  //aqui esta no exit IR

  WRITEPIN(PIN_TMS, HIGH); JTAG_clock();
  WRITEPIN(PIN_TMS, LOW); JTAG_clock();

  //aqui esta no IDLE

  //espera
  for (n = 0; n < 200; n++)
  {
    JTAG_clock();
  }

  JTAG_reset();

}


int jtagProgram(const char* coreName) {
  unsigned long bitcount = 0;
  bool last = false;
  int n = 0;

  shell_println ("Programming ");
  JTAG_PREprogram();

  FsFile sdfile; 
  sdfile.open(coreName);

  if (!sdfile) {
    shell_printf ("error: file open failed.\n");

    return SHELL_RET_FAILURE;

  }

  int mark_pos = 0;
  unsigned long total = sdfile.size();

  printf("total %lu bytes\n", total);
  int divisor = total / 32;
  int state = LOW;

  while (bitcount < total)  {
    unsigned char val = sdfile.read();
    int value = 0;

    
    if (bitcount % divisor == 0)
    {
      onValueChanged(100*bitcount/total);
      shell_printf ("*");
    }

    bitcount++;

    for (n = 0; n <= 7; n++)
    {
      value = ((val >> n) & 0x01);
      WRITEPIN(PIN_TDI, value); JTAG_clock();
    }
  }
  shell_printf(" FINISH \n");

  //writetofile ("Additional 16 bytes of 0xFF ------------------------------------------");
  /* AKL (Version1.7): Dump additional 16 bytes of 0xFF at the end of the RBF file */
  for (n = 0; n < 127; n++ ) {
    WRITEPIN(PIN_TDI, HIGH); JTAG_clock();
  }

  WRITEPIN(PIN_TDI, HIGH);
  WRITEPIN(PIN_TMS, HIGH);
  JTAG_clock();

  printf("Programmed %lu bytes\n", bitcount);
  sdfile.close();

  JTAG_POSprogram();

  return SHELL_RET_SUCCESS;
}
