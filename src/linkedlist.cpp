#include "linkedlist.h"


NodeList *linkedListAddNode(NodeList *head, char val[LIST_ITEM_NAME_LENGTH]) {
    NodeList *current = head;
    NodeList *newNode = (NodeList *) malloc (sizeof(NodeList));
    strcpy(newNode->name,val);
    newNode->next = NULL;
    if (current == NULL){
      head = newNode;
    }
    else{
      while (current->next != NULL) {
          current = current->next;
      }
      current->next = newNode;
    }
    //Serial.printf("Aded node %s\n", newNode->name);
    return head;
}

void linkedListRemoveNode(NodeList * head) {
    NodeList *current = head;
    NodeList *prev = current;
    while (current->next != NULL) {
        prev = current;
        current = current->next;
    }
    free (current);
    prev->next=NULL;    
}

void linkedListConcat( NodeList **l0, NodeList *l1 )
{
    while (*l0 != NULL)
       l0 = &((*l0)->next);
   
    *l0 = l1;
}

void linkedListClear(NodeList *head){  
    NodeList *current = head;
    NodeList *prev = current;
    while (current != NULL) {
        prev = current;
        current = current->next;
        free(prev);      
    }
}

void linkedListSort( NodeList **list )
{    
    /* Sorting an empty list is trivial */
    if (!*list)
        return;
 
    /* Extract the pivot */    
    NodeList *pivot = *list;
    char *name = pivot->name;
    NodeList *p = pivot->next;
    pivot->next = NULL;

    /* Construct left and right lists in place in a single pass*/
    NodeList *left = NULL;
    NodeList *right = NULL;

    while (p)
    {
        NodeList *n = p;  
        p = p->next;
        
        if (strcasecmp(n->name,name)<0)
            left=linkedListAddNode( left, n->name );
        else
            right=linkedListAddNode( right, n->name );
    }
 
    /* We now sort left and right */
    /* If left and right are of vastly different lengths, the complexity won't be O(n log n) */
    linkedListSort( &left );
    linkedListSort( &right );

    /* We now concatenate lists [this is inefficient, but doesn't hurt complexity] */
    NodeList *result = NULL;
    linkedListConcat( &result, left );
    linkedListConcat( &result, pivot );
    linkedListConcat( &result, right );
    *list = result;
}
