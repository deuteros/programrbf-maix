 
/**
  BSD 3-Clause License

  Copyright (c) 2021, Guillermo Amat
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIST_ITEM_NAME_LENGTH 256

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Linked list structure. Stores a character array as data. 
 */
typedef struct Node {
    char name[LIST_ITEM_NAME_LENGTH];
    struct Node* next;
} NodeList;

/**
 * Adds a node at the end of the list. If the list is empty, allocates memory and creates the first node
 * 
 * @param head Linked list to add a node
 * @param val Value or data of the new node.
 * @return A pointer to the list including the new node at the end.
 **/
NodeList * linkedListAddNode(NodeList * head, char val[256]);

/**
 * Removes the last node of a linked list.
 * 
 * @param head List to be reduced in one item (node)
 * 
 **/
void linkedListRemoveNode(NodeList * head);

/** 
 * This function joins two existing lists. The second one is appended to the first.
 * @param l0 First one of the lists. The second list will be added at the end of this.
 * @param l1 List to be added to the first one
 * 
 **/
void linkedListConcat( NodeList **l0, NodeList *l1 );

/**
 * Removes and frees memory of all the nodes of a list
 * @param head List to be cleared.
 *
 **/
void linkedListClear(NodeList *head);

/**
 * Qsort algorithm that works with a linked list. The code was originally found here:  https://gist.github.com/nnathan/7b8ca118f56ce84627f6a8d4b68bcfb1
 * The comparison methos has been changed to order strings (case insensitive)
 * @param list The list to be ordered alphabetically
 * 
 **/
void linkedListSort( NodeList **list );
    
#ifdef __cplusplus
}
#endif

#endif //LINKEDLIST_H 
 
