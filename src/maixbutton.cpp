/**
  BSD 3-Clause License

  Copyright (c) 2021, Guillermo Amat
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#include "maixbutton.h"

// constants won't change. They're used here to set pin numbers:
#ifdef K210
const int MAIX_BUTTON_PIN = KEY0; // the number of the pushbutton pin
#endif
#ifdef RASPBERRY_PI_PICO
const int MAIX_BUTTON_PIN = 20;
#else
const int MAIX_BUTTON_PIN = 0;
#endif

const int MAIX_BUTTON_LONG_PRESS_TIME = 500; // 500 milliseconds

// Variables will change:
int buttonLastState = HIGH;  // the previous state from the input pin
int buttonCurrentState;     // the current reading from the input pin
unsigned long buttonPressedTime  = 0;
unsigned long buttonReleasedTime = 0;


buttonObjectEvent onPressed;
buttonObjectEvent onLongPressed;

void buttonSetup() {
  pinMode(MAIX_BUTTON_PIN, INPUT_PULLUP);
}

void buttonEvent() {
  // read the state of the switch/button:
  buttonCurrentState = digitalRead(MAIX_BUTTON_PIN);

  if (buttonLastState == HIGH && buttonCurrentState == LOW)       // button is pressed
    buttonPressedTime = millis();
  else if (buttonLastState == LOW && buttonCurrentState == HIGH) { // button is released
    buttonReleasedTime = millis();

    long pressDuration = buttonReleasedTime - buttonPressedTime;

    if ( pressDuration < MAIX_BUTTON_LONG_PRESS_TIME ) {
      Serial.println("A short press is detected");
      onPressed(NULL, 1);
    }else{
      Serial.println("A long press is detected");
      onLongPressed(NULL, 2);
      
    }
  }

  // save the the last state
  buttonLastState = buttonCurrentState;
}
