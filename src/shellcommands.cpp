/**
  BSD 3-Clause License

  Copyright (c) 2021, Guillermo Amat
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#include "shellcommands.h"
#include "jtag.h"


extern const char *FileNameRBF;

int command_idcode(int argc, char** argv)
{
  shell_println("Running \"mycommand\" now");

  jtagSetup( );
  jtagScan( );
  jtagRelease();
  shell_println("Exit...");
  return SHELL_RET_SUCCESS;
}

int command_programrbf(int argc, char** argv)
{

  if ( argc != 2 ) {
    //analogWrite(PIN_LED_GREEN , 0);
    shell_printf("Use: %s [rbf_file] TCK TDI TMS TDO \n", argv[0]);
    shell_printf("Example on Atlas-FPGA: \n");
    shell_printf("%s msx_atlas.rbf 18 19 20 21\n\n", argv[0]);
  } else {
    shell_printf( "Programming the file %s \n", argv[1]);
    const char *coreName = argv[1];

    jtagSetup( );
    if ( jtagScan( ) == 0 ) {
      unsigned int startTime = millis();
      unsigned int duration = 0;
      if (jtagProgram(coreName) == SHELL_RET_SUCCESS) {
        duration = millis() - startTime;
        shell_printf( "Programming time: %d ms\n", duration);
      } else {
        shell_printf( "Something went wrong while programming the fpga\n");
      }

    }
    jtagRelease();

    shell_printf( "OK, finished\n" );
  }

  return SHELL_RET_SUCCESS;

}
